# Test unbalanced classification

The dataset is Statlog (Shuttle) from:

https://archive.ics.uci.edu/ml/datasets/Statlog+%28Shuttle%29

which contains 9 numerical features and the target is labeled in 7 groups with approximately 80% of the observations belonging to class 1. Classes 6 and 7 have respectively 6 and 11 observations among 58000 total observations.

## Goal

This notebook aims to test some classifiers when there is an unbalanced target.

## Main Contents 

- Unbalanced Data
- Logistic Regression
- Random Forest Classifier
- Extra Tree Classifier
- KNeighbors Classifier
- Gradient Boosting Classifier
- Voting Classifier

## Requirements

The requirements file contains all the packages needed to work through this notebook
